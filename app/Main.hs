module Main where

import Data.Functor ((<&>))
import Data.List (intercalate)

import System.Random (RandomGen, mkStdGen, next)
import Safe (atMay)
import Data.List.Tools (setAt)
import System.Console.ANSI (clearScreen)
import Control.Concurrent (threadDelay)

import Data.Matrix
import qualified Data.Vector             as V

{-

#S####################################
#                                    #
#                                    #
#                                    #
#                                    #
#                                    #
#                                    #
#                                    #
####################################E#

#S####################################
# #    #                             #
#   # ##                             #
#####      #                         #
#          #                         #
#                                    #
##        # #                        #
#         # #                        #
####################################E#


-}

data Pixel = Air
           | Wall
           | Start
           | End
           | Inaccessible

instance Show Pixel where
  show Air   = "  "
  show Wall  = "██"
  show Start = "SS"
  show End   = "EE"
  show Inaccessible = "XX"


newtype Labyrinth = Labyrinth (Matrix Pixel)
--data Labyrinth = Labyrinth {
--    width  :: Int
--  , height :: Int
--  , world  :: [[Pixel]]
--  }

width :: Labyrinth -> Int
width (Labyrinth m) = ncols m

height :: Labyrinth -> Int
height (Labyrinth m) = nrows m

instance Show Labyrinth where
  --show (Labyrinth m) = prettyMatrix m
  show (Labyrinth m) = prettyMatrixNoSpace m

prettyMatrixNoSpace :: Show a => Matrix a -> String
prettyMatrixNoSpace m = concat
   [ "┌ ", concat (replicate (ncols m) blank), " ┐\n"
   , unlines
   [ "│ " ++ concat (fmap (\j -> fill $ strings ! (i,j)) [1..ncols m]) ++ " │" | i <- [1..nrows m] ]
   , "└ ", concat (replicate (ncols m) blank), " ┘"
   ]
 where
   strings  = fmap show m
   v = getMatrixAsVector strings
   widest = V.maximum $ fmap length v
   fill str = replicate (widest - length str) ' ' ++ str
   blank = fill ""


--                       x      y          
getPixel :: Labyrinth -> Int -> Int -> Pixel
getPixel (Labyrinth m) x y = case safeGet y x m of
  Just p -> p
  Nothing -> Inaccessible

setPixel :: Labyrinth -> Int -> Int -> Pixel -> Labyrinth
setPixel lab@(Labyrinth m) x y p = case safeSet p (y,x) m of
  Just nm -> Labyrinth nm
  Nothing -> lab

initLab :: Int -> Int -> Labyrinth
initLab w h = Labyrinth (matrix h w (initPixel w h))

initPixel :: Int -> Int -> (Int, Int) -> Pixel
initPixel w h (y,x) | y == 1 && x == 3 = Start
                    | y == h && x == w-2 = End
                    | x == 1 = Wall
                    | y == 1 = Wall
                    | x == w = Wall
                    | y == h = Wall
                    | otherwise = Air

tryBuild :: Labyrinth -> Int -> Int -> Labyrinth
tryBuild lab x y = if buildable lab x y
                   then setPixel lab x y Wall
                   else lab


-- 012
-- 345
-- 678
getNeighbours :: Labyrinth -> Int -> Int -> (Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel)
getNeighbours lab x y = (
    getPixel lab (x-1) (y-1)
  , getPixel lab (x+0) (y-1)
  , getPixel lab (x+1) (y-1)
  , getPixel lab (x-1) (y+0)
  , getPixel lab (x+0) (y+0)
  , getPixel lab (x+1) (y+0)
  , getPixel lab (x-1) (y+1)
  , getPixel lab (x+0) (y+1)
  , getPixel lab (x+1) (y+1)
  )

buildable :: Labyrinth -> Int -> Int -> Bool
buildable lab x y = buildable' $ getNeighbours lab x y

--  N 
-- W E
--  S
buildable' :: (Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel,Pixel) -> Bool
buildable' (_    ,Wall,_    -- North
           ,Air  ,Air ,Air
           ,Air  ,Air ,Air) = True
buildable' (Air  ,Air ,Air  -- South
           ,Air  ,Air ,Air
           ,_    ,Wall,_  ) = True
buildable' (_    ,Air ,Air -- West
           ,Wall ,Air ,Air
           ,_    ,Air ,Air  ) = True
buildable' (Air  ,Air ,_   -- East
           ,Air  ,Air ,Wall
           ,Air  ,Air ,_  ) = True
buildable' _ = False

main :: IO ()
main = do
  let gen = mkStdGen 12346
  putStrLn "Hello, Maus!"
  putStrLn "Hello from the other side!"
  putStrLn "Hello, Haskell!"
  let lab = initLab 20 20
  print lab
  -- putStrLn $ "Pixel: '" ++ (show $ getPixel lab 20 0) ++ "'"
  loop gen lab 0


loop :: RandomGen g => g -> Labyrinth -> Int -> IO ()
loop gen lab tick = do
  clearScreen
  let (rx,ngen) = next gen
  let x = rx `mod` width lab
  let (ry,nngen) = next ngen
  let y = ry `mod` height lab
  let nlab = tryBuild lab x y
  if (tick `mod` 20 == 0)
  then do
    putStrLn $ "X: " ++ (show x) ++ " Y: " ++ (show y)
    print nlab
    threadDelay 100000
  else mempty
  loop nngen nlab (tick+1)





